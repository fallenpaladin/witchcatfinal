﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartWater : MonoBehaviour
{
    // Start is called before the first frame update

    

    [SerializeField] GameObject player;
    MagiaAgua water;
    void Start()
    {
        water = player.GetComponent<MagiaAgua>();
        ActiveWater();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ActiveWater()
    {
        water.enabled = !water.enabled;
    }
}
