﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagiaAgua : MonoBehaviour {
	public static MagiaAgua Instance;
	Animator animator;
	Collider2D[] colisor;
	Rigidbody2D rb;
	[SerializeField]
	public float forcaAgua;

	// Use this for initialization
	void Awake(){
		Instance = this;
	}
	void Start () {
		animator = gameObject.GetComponent<Animator>();
		rb = gameObject.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.UpArrow)){
			StartCoroutine(Poder());
		}
	}

	public IEnumerator Poder(){
		if(Input.GetKeyDown(KeyCode.UpArrow)){
			animator.SetTrigger("Agua");
			colisor = new Collider2D[3];
			transform.Find("PoderAgua").gameObject.GetComponent<Collider2D>()
			 .OverlapCollider(new ContactFilter2D(), colisor);
			for(int i = 0; i < colisor.Length; i++){
				Debug.Log("col de i" + colisor[i]);
				if(colisor[i]!=null && colisor[i].gameObject.CompareTag("Fogo")){
					yield return new WaitForSeconds(1.0f);
					Destroy(colisor[i].gameObject);
				}else if(colisor[i]!=null && colisor[i].gameObject.CompareTag("Enemy")){
					colisor[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(forcaAgua , 0.0f));
				}
			}
		}
	}
}
