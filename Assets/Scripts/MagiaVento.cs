﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagiaVento : MonoBehaviour {
	Animator animator;
	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.DownArrow)){
			PoderVento();
		}
	}

	public void PoderVento(){
		animator.SetTrigger("Vento");
	}
}
